function Calcularlos() {
  var elemento_1 = document.getElementById("largo");
  valor_1 = parseFloat(elemento_1.value).toFixed(2);
  var elemento_2 = document.getElementById("ancho");
  valor_2 = parseFloat(elemento_2.value).toFixed(2);
  var elemento_3 = document.getElementById("alto");
  valor_3 = parseFloat(elemento_3.value).toFixed(2);

  area = (valor_1 * valor_2).toFixed(2);

  var e = document.getElementById("zona");
  console.log(e);
  var zzzona = e.value;
  console.log(zzzona);

  cubico = parseFloat(area * valor_3).toFixed(2);
  frigorias = parseFloat(cubico * 75).toFixed(2);
  resultado_frigo =
    "El area cubierta es de " +
    area +
    " metros cuadrados." +
    "<br><br>Los metros cubicos son: " +
    cubico +
    "." +
    "<br><br>Las frigorias necesarias para enfriar dicho espacio es de: " +
    frigorias +
    " como minimo.";
  document.getElementById("resultado").innerHTML = resultado_frigo;
}

function validador_previo() {
  var elemento_1 = document.getElementById("largo").value;
  var elemento_2 = document.getElementById("ancho").value;
  var elemento_3 = document.getElementById("alto").value;
  if (elemento_1 == "" || elemento_2 == "" || elemento_3 == "") {
    alert("Ningun campo puede quedar vacio");
    return false;
  } else {
    validador_num();
  }
}

function isNumber(num) {
  return !isNaN(parseFloat(num)) && isFinite(num);
}

function validador_num() {
  var elemento_1 = document.getElementById("largo");
  valor_1 = parseFloat(elemento_1.value).toFixed(2);
  var elemento_2 = document.getElementById("ancho");
  valor_2 = parseFloat(elemento_2.value).toFixed(2);
  var elemento_3 = document.getElementById("alto");
  valor_3 = parseFloat(elemento_3.value).toFixed(2);
  var reg = /^\d*\.?\d+$/;
  if (isNumber(valor_1) && isNumber(valor_2) && isNumber(valor_3)) {
    Calcularlos();
  } else {
    alert("Solo se admiten números");
    return false;
  }
}
