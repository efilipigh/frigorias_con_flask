from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def inicio():
    return render_template('home.html')

@app.route('/acerca')
def acerca():
    return render_template('about.html')

@app.route('/frigorias')
def frigorias():
    return render_template('frigorias.html')

if __name__ == '__main__':
    #app.run()
    app.run("0.0.0.0",8080,debug=True)